# Seven Answers

This repository contains data files (in CSV format) and R code (in R
Markdown files) used to produce plots and related material for the
"Seven Answers" series of blog posts at civilityandtruth.com.

There is a subdirectory for each of the posts for which I've created
plots.
